/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Marius
 */
@Entity
@Table(name = "CANDIDATE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Candidate.findAll", query = "SELECT c FROM Candidate c"),
    @NamedQuery(name = "Candidate.findByCandidateid", query = "SELECT c FROM Candidate c WHERE c.candidateid = :candidateid"),
    @NamedQuery(name = "Candidate.findByPhonenumber", query = "SELECT c FROM Candidate c WHERE c.phonenumber = :phonenumber"),
    @NamedQuery(name = "Candidate.findByAddress", query = "SELECT c FROM Candidate c WHERE c.address = :address"),
    @NamedQuery(name = "Candidate.findByEmail", query = "SELECT c FROM Candidate c WHERE c.email = :email"),
    @NamedQuery(name = "Candidate.findByCv", query = "SELECT c FROM Candidate c WHERE c.cv = :cv"),
    @NamedQuery(name = "Candidate.findByAccountid", query = "SELECT c FROM Candidate c WHERE c.accountid = :accountid"),
    @NamedQuery(name = "Candidate.findByPositionid", query = "SELECT c FROM Candidate c WHERE c.positionid = :positionid")})
public class Candidate implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CANDIDATEID")
    private Integer candidateid;
    @Column(name = "PHONENUMBER")
    private String phonenumber;
    @Column(name = "ADDRESS")
    private String address;
    @Column(name = "EMAIL")
    private String email;
    @Column(name = "CV")
    private String cv;
    @Column(name = "ACCOUNTID")
    private Integer accountid;
    @Column(name = "POSITIONID")
    private Integer positionid;

    public Candidate() {
    }

    public Candidate(Integer candidateid) {
        this.candidateid = candidateid;
    }

    public Integer getCandidateid() {
        return candidateid;
    }

    public void setCandidateid(Integer candidateid) {
        this.candidateid = candidateid;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCv() {
        return cv;
    }

    public void setCv(String cv) {
        this.cv = cv;
    }

    public Integer getAccountid() {
        return accountid;
    }

    public void setAccountid(Integer accountid) {
        this.accountid = accountid;
    }

    public Integer getPositionid() {
        return positionid;
    }

    public void setPositionid(Integer positionid) {
        this.positionid = positionid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (candidateid != null ? candidateid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Candidate)) {
            return false;
        }
        Candidate other = (Candidate) object;
        if ((this.candidateid == null && other.candidateid != null) || (this.candidateid != null && !this.candidateid.equals(other.candidateid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.Candidate[ candidateid=" + candidateid + " ]";
    }
    
}
