/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Marius
 */
@Entity
@Table(name = "INTERVIEW")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Interview.findAll", query = "SELECT i FROM Interview i"),
    @NamedQuery(name = "Interview.findByInterviewid", query = "SELECT i FROM Interview i WHERE i.interviewid = :interviewid"),
    @NamedQuery(name = "Interview.findByPassed", query = "SELECT i FROM Interview i WHERE i.passed = :passed"),
    @NamedQuery(name = "Interview.findByInterviewdate", query = "SELECT i FROM Interview i WHERE i.interviewdate = :interviewdate"),
    @NamedQuery(name = "Interview.findByCandidateid", query = "SELECT i FROM Interview i WHERE i.candidateid = :candidateid")})
public class Interview implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "INTERVIEWID")
    private Integer interviewid;
    @Column(name = "PASSED")
    private Boolean passed;
    @Column(name = "INTERVIEWDATE")
    @Temporal(TemporalType.DATE)
    private Date interviewdate;
    @Column(name = "CANDIDATEID")
    private Integer candidateid;

    public Interview() {
    }

    public Interview(Integer interviewid) {
        this.interviewid = interviewid;
    }

    public Integer getInterviewid() {
        return interviewid;
    }

    public void setInterviewid(Integer interviewid) {
        this.interviewid = interviewid;
    }

    public Boolean getPassed() {
        return passed;
    }

    public void setPassed(Boolean passed) {
        this.passed = passed;
    }

    public Date getInterviewdate() {
        return interviewdate;
    }

    public void setInterviewdate(Date interviewdate) {
        this.interviewdate = interviewdate;
    }

    public Integer getCandidateid() {
        return candidateid;
    }

    public void setCandidateid(Integer candidateid) {
        this.candidateid = candidateid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (interviewid != null ? interviewid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Interview)) {
            return false;
        }
        Interview other = (Interview) object;
        if ((this.interviewid == null && other.interviewid != null) || (this.interviewid != null && !this.interviewid.equals(other.interviewid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.Interview[ interviewid=" + interviewid + " ]";
    }
    
}
