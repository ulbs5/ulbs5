/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Marius
 */
@Entity
@Table(name = "POSITIONS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Positions.findAll", query = "SELECT p FROM Positions p"),
    @NamedQuery(name = "Positions.findByPositionid", query = "SELECT p FROM Positions p WHERE p.positionid = :positionid"),
    @NamedQuery(name = "Positions.findByName", query = "SELECT p FROM Positions p WHERE p.name = :name"),
    @NamedQuery(name = "Positions.findByAvailability", query = "SELECT p FROM Positions p WHERE p.availability = :availability"),
    @NamedQuery(name = "Positions.findByDetails", query = "SELECT p FROM Positions p WHERE p.details = :details"),
    @NamedQuery(name = "Positions.findAvailablePositions", query = "SELECT p FROM Positions p WHERE p.availability = :availability")})
public class Positions implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "POSITIONID")
    private Integer positionid;
    @Column(name = "NAME")
    private String name;
    @Column(name = "AVAILABILITY")
    private Boolean availability;
    @Column(name = "DETAILS")
    private String details;

    public Positions() {
    }

    public Positions(Integer positionid) {
        this.positionid = positionid;
    }

    public Integer getPositionid() {
        return positionid;
    }

    public void setPositionid(Integer positionid) {
        this.positionid = positionid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getAvailability() {
        return availability;
    }

    public void setAvailability(Boolean availability) {
        this.availability = availability;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (positionid != null ? positionid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Positions)) {
            return false;
        }
        Positions other = (Positions) object;
        if ((this.positionid == null && other.positionid != null) || (this.positionid != null && !this.positionid.equals(other.positionid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.Positions[ positionid=" + positionid + " ]";
    }
    
}
