/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

/**
 *
 * @author Marius
 */
public class PositionDetails implements java.io.Serializable {
    private Integer positionId;
    private String name;
    private Boolean availability;
    private String details;
    
    public PositionDetails(
            Integer positionId,
            String name,
            Boolean availability,
            String details
    ){
        this.positionId=positionId;
        this.name=name;
        this.availability=availability;
        this.details=details;
    }
    
    public Integer getId(){
        return this.positionId;
    }
    
    public String getName(){
        return this.name;
    }
    
    public Boolean getAvailability(){
        return this.availability;
    }
    
    public String getDetails(){
        return this.details;
    }
    
    @Override
    public String toString(){
        String s=positionId+" "+name+" "+availability+" "+details;
        return s;
    }
}
