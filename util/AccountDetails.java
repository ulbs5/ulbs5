/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

/**
 *
 * @author Marius
 */
public class AccountDetails implements java.io.Serializable{
    private Integer accountId;
    private String username;
    private String password;
    private Integer rights;
    private String firstname;
    private String lastname;
    
    public AccountDetails(
           Integer accountId,
           String username,
           String password,
           Integer rights,
           String firstname,
           String lastname
    ){
        this.accountId=accountId;
        this.username=username;
        this.password=password;
        this.rights=rights;
        this.firstname=firstname;
        this.lastname=lastname;
    }
    
    public Integer getId(){
        return this.accountId;
    }
    
    public String getUsername(){
        return this.username;
    }
    
    public String getPassword(){
        return this.password;
    }
    
    public Integer getRights(){
        return this.rights;
    }
    
    public String getFirstName(){
        return this.firstname;
    }
    
    public String getLastName(){
        return this.lastname;
    }
    
    @Override
    public String toString(){
        String s=accountId + " " + username+" "+password+" "+rights+" "+firstname+" "+lastname;
        return s;
    }
}
