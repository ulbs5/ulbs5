/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

/**
 *
 * @author Marius
 */
public class CandidateDetails implements java.io.Serializable {
    private Integer candidateId;
    private Integer accountId;
    private Integer positionId;
    private String phonenumber;
    private String email;
    private String address;
    private String cv;
    
    public CandidateDetails(
            Integer candidateId,
            Integer accountId,
            Integer positionId,
            String phonenumber,
            String email,
            String address,
            String cv
    ){
        this.candidateId=candidateId;
        this.accountId=accountId;
        this.positionId=positionId;
        this.phonenumber=phonenumber;
        this.email=email;
        this.address=address;
        this.cv=cv;
    }
    
    public Integer getId(){
        return this.candidateId;
    }
    
    public Integer getAccountId(){
        return this.accountId;
    }
    
    public Integer getPositionId(){
        return this.positionId;
    }
    
    public String getPhoneNmber(){
        return this.phonenumber;
    }
    
    public String getAddress(){
        return this.address;
    }
    
    public String getEmail(){
        return this.email;
    }
    
    public String getCV(){
        return this.cv;
    }
    
    @Override
    public String toString(){
        String s=candidateId+" "+accountId+" "+positionId+" "+phonenumber+" "+email+" "+address+" "+cv;
        return s;
    }
}
