/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.util.Date;

/**
 *
 * @author Marius
 */
public class InterviewDetails implements java.io.Serializable {
    private Integer interviewId;
    private Boolean passed;
    private Date interviewDate;
    
    public InterviewDetails(
            Integer interviewId,
            Boolean passed,
            Date interviewDate
    ){
        this.interviewId=interviewId;
        this.passed=passed;
        this.interviewDate=interviewDate;
    }
    
    public Integer getId(){
        return this.interviewId;
    }
    
    public Boolean getPassed(){
        return this.passed;
    }
    
    public Date getInterviewDate(){
        return this.interviewDate;
    }
    
    @Override
    public String toString(){
        String s=interviewId+" "+passed+" "+interviewDate;
        return s;
    }
}
