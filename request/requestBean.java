/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package request;

import java.util.Date;
import java.util.List;
import javax.ejb.Stateful;
import util.AccountDetails;
import util.CandidateDetails;
import util.InterviewDetails;
import util.PositionDetails;
import entity.Accounts;
import entity.Candidate;
import entity.Interview;
import entity.Positions;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.logging.Logger;
import java.util.List;
import java.util.Iterator;
import javax.ejb.EJBException;

/**
 *
 * @author Marius
 */
@Stateful
public class requestBean implements request {
    private static final Logger logger=Logger.getLogger("request.requestBean");
    @PersistenceContext
    private EntityManager em;

    @Override
    public void addAccount(Integer acountId, String username, String password, Integer rights, String fistname, String lastname) {
       logger.info("Add Account");
        try{
           Accounts account=new Accounts();
           account.setAccountid(acountId);
           account.setUsername(username);
           account.setPassword(password);
           account.setRights(rights);
           account.setFirstname(fistname);
           account.setLastname(lastname);
           em.persist(account);
       }
       catch(Exception ex){
           throw new EJBException (ex);
       }
    }

    @Override
    public void addCandidate(Integer candidateId, Integer accountId, Integer positionId, String phonenumber, String address, String email, String cv) {
       logger.info("Add Candidate");
        try {
            Candidate candidate= new Candidate();
            candidate.setCandidateid(candidateId);
            candidate.setAccountid(accountId);
            candidate.setPositionid(positionId);
            candidate.setPhonenumber(phonenumber);
            candidate.setAddress(address);
            candidate.setEmail(email);
            candidate.setCv(cv);
            em.persist(candidate);
            
        } catch (Exception e) {
            throw new EJBException(e);
        }
    }

    @Override
    public void addInterview(Integer interviewId, Date interviewDate, Integer accountid) {
        logger.info("Add Interview");
        try {
            Interview interview=new Interview();
            Candidate candidate=em.find(Candidate.class, accountid);
            interview.setInterviewid(interviewId);
            interview.setInterviewdate(interviewDate);
            interview.setPassed(null);
            interview.setCandidateid(candidate.getCandidateid());
            em.persist(interview);
        } catch (Exception e) {
            throw new EJBException(e);
        }
    }

    @Override
    public void addPosition(Integer positionId, String name, Boolean availability, String details) {
       logger.info("Add Position");
        try {
            Positions position=new Positions();
            position.setPositionid(positionId);
            position.setName(name);
            position.setAvailability(availability);
            position.setDetails(details);
        } catch (Exception e) {
            throw new EJBException(e);
        }
    }

    @Override
    public List<CandidateDetails> getAllCandidates() {
        logger.info("GetAllCanidates");
        List<CandidateDetails> candidateDetails=null;
        try {
            candidateDetails = em.createNamedQuery("entity.Candidate.findAll").getResultList();
            return candidateDetails;
        } catch (Exception e) {
            throw new EJBException(e);
        }
    }

    @Override
    public List<CandidateDetails> getCandidates(Integer positionId) {
       logger.info("GetCandidatesByPositions");
       List<CandidateDetails> candidateDetails=null;
        try {
            candidateDetails = em.createNamedQuery("entity.Candidate.findByPositionid").setParameter("positionid", positionId).getResultList();
            return candidateDetails;
        } catch (Exception e) {
            throw new EJBException(e);
        }
    }

    @Override
    public CandidateDetails getCandidate(Integer candidateId) {
        logger.info("GetCandidate");
        CandidateDetails candidateDetails=null;
        try {
            Candidate candidate=em.find(Candidate.class, candidateId);
            candidateDetails=new CandidateDetails(candidate.getCandidateid(), candidate.getAccountid(), candidate.getPositionid(), candidate.getPhonenumber(), candidate.getEmail(), candidate.getAddress(), candidate.getCv());
            return candidateDetails;
        } catch (Exception e) {
            throw new EJBException(e);
        }
    }

    @Override
    public List<PositionDetails> getAllPositions() {
        logger.info("GetAllPositions");
        List<PositionDetails> positionsDetails=null;
        try {
            positionsDetails=em.createNamedQuery("entity.Positions.findAll").getResultList();
            return positionsDetails;
        } catch (Exception e) {
            throw new EJBException(e);
        }
    }

    @Override
    public List<PositionDetails> getAvailablePositions() {
        logger.info("GetAvailablePositions");
        List<PositionDetails> positionsAvailable=null;
        try {
            positionsAvailable=em.createNamedQuery("entity.Positions.findAvailablePositions").setParameter("availability", true).getResultList();
            return positionsAvailable;
        } catch (Exception e) {
            throw new EJBException(e);
        }
    }

    @Override
    public PositionDetails getPosition(Integer positionId) {
        logger.info("GetASpecificPosition");
        PositionDetails position=null;
        try {
            position=(PositionDetails)em.createNamedQuery("entity.Positions.findByPositionid").setParameter("positionid", positionId).getResultList();
            return position;
        } catch (Exception e) {
            throw new EJBException(e);
        }
    }

    @Override
    public List<InterviewDetails> getAllInterviews() {
        logger.info("GetAllInterviews");
        List<InterviewDetails> interviewsDetails=null;
        try {
            interviewsDetails=em.createNamedQuery("entity.Interview.findAll").getResultList();
            return interviewsDetails;
        } catch (Exception e) {
            throw new EJBException(e);
        }
    }

    @Override
    public List<InterviewDetails> getAllInterviewsOn(Date interviewDate) {
        logger.info("GetAllInterviesByDate");
        List<InterviewDetails> interviews=null;
        try {
            interviews=em.createNamedQuery("entity.Interview.findByInterviewdate").setParameter("interiewdate", interviewDate).getResultList();
            return interviews;
        } catch (Exception e) {
            throw new EJBException(e);
        }
    }

    @Override
    public void removeCandidate(Integer candidateId) {
       logger.info("RevemoveCandidate");
       Candidate candidate=null;
       Interview interview=null;
        try {
            candidate=em.find(Candidate.class, candidateId);
            em.remove(candidate);
            interview=em.find(Interview.class, candidateId);
            em.remove(interview);
        } catch (Exception e) {
            throw new EJBException(e);
        }
    }

    @Override
    public void closePosition(Integer positionId) {
        logger.info("CloasePosition");
        Positions position=null;
        try {
            position=em.find(Positions.class, positionId);
            em.getTransaction().begin();
            position.setAvailability(Boolean.FALSE);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new EJBException(e);
        }
    }

    @Override
    public void openExistingPosition(Integer positionId) {
        logger.info("OpenExistingPostion");
        Positions position=null;
        try {
            position=em.find(Positions.class, positionId);
            em.getTransaction().begin();
            position.setAvailability(Boolean.TRUE);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new EJBException(e);
        }
    }

    @Override
    public void removePosition(Integer positionId) {
        logger.info("RemovePosition");
        Positions position=null;
        try {
            position=em.find(Positions.class, positionId);
            em.remove(position);
        } catch (Exception e) {
            throw new EJBException(e);
        }
    }

    @Override
    public void setResult(Integer candidateId, Boolean result) {
        logger.info("SetResult");
        Interview interview=null;
        try {
            interview=em.find(Interview.class, candidateId);
            em.getTransaction().begin();
            interview.setPassed(result);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new EJBException(e);
        }
    }
}
