/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package request;
import java.util.List;
import javax.ejb.Remote;
import util.*;
import java.util.Date;

/**
 *
 * @author Marius
 */
@Remote
public interface request {
    void addAccount(
        Integer acountId, 
        String username, 
        String password, 
        Integer rights,
        String firstname,
        String lastname);
    
    void addCandidate(
        Integer candidateId,
        Integer accountId,
        Integer positionId,
        String phonenumber,
        String address,
        String email,
        String cv);
    
    void addInterview(
        Integer interviewId,
        Date interviewDate,
        Integer candidateId);
    
    void addPosition(
        Integer positionId,
        String name,
        Boolean availability,
        String details);
    
    List<CandidateDetails> getAllCandidates();
    
    List<CandidateDetails> getCandidates(Integer positionId);
    
    CandidateDetails getCandidate(Integer candidateId);
    
    List<PositionDetails> getAllPositions();
    
    List<PositionDetails> getAvailablePositions();
    
    PositionDetails getPosition(Integer positionId);
    
    List<InterviewDetails> getAllInterviews();
    
    List<InterviewDetails> getAllInterviewsOn(Date interviewDate);
    
    void removeCandidate(Integer candidateId);
    
    void closePosition(Integer positionId);
    
    void openExistingPosition(Integer positionId);
    
    void removePosition(Integer positionId);
    
    void setResult(Integer candidateId, Boolean result);
}
